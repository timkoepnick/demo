package com.example.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HalloController {

    @GetMapping("/hallo")
    @CrossOrigin
    public ResponseEntity<String> halloGet() {
        System.out.println("hier ist echt was angekommen aus ner dart app");
        return ResponseEntity.status(HttpStatus.OK).body("Gruß ausm Spring Boot Bacend");
    }

}
